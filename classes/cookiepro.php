<?php

/**
 * @module          CookiePro
 * @author          cms-lab
 * @copyright       2019-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookiepro/license.php
 * @license_terms   please see license
 *
 */

class cookiepro extends LEPTON_abstract
{
	public array $settings = [];
	public array $current_page = [];
	public array $all_cmp = [];
	public array $cmp_code = [];
	public string $addon_color = 'blue';
	public string $action_url = ADMIN_URL . '/admintools/tool.php?tool=cookiepro';	
	
	public object|null $oTwig = null;
	public LEPTON_admin $admin;
	public LEPTON_database $database;
	public static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->admin = LEPTON_admin::getInstance();
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('cookiepro');		
		$this->init_tool();	
	}
	
	public function init_tool( $sToolname = '' )
	{
		//get cmp
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_cookiepro_cmp " ,
			true,
			$this->all_cmp,
			true
		);		
	}

	public function list_cmp() 
	{
		// data for twig template engine	
		$data = array(
			'oCP'			=> $this,
			'readme_link'	=> 'https://cms-lab.com/_documentation/cookiepro/readme.php',
			'leptoken'		=> get_leptoken()			
			);

		// get the template-engine	
		echo $this->oTwig->render( 
			"@cookiepro/list.lte",	//	template-filename
			$data						//	template-data
		);			
	}

	public function edit_cmp( $cmp_ID = -1) 
	{	
		if($_POST['edit_cmp'] > 0)
		{
			$cmp_id = intval($_POST['edit_cmp']);
		}

		//get code
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_cookiepro" ,
			true,
			$this->cmp_code,
			false
		);
		
		// data for twig template engine	
		$data = array(
			'oCP'			=> $this,
			'cmp_id'		=> $cmp_id,
			'leptoken'		=> get_leptoken()			
			);
		
		// get the template-engine	
		echo $this->oTwig->render( 
			"@cookiepro/edit.lte",	//	template-filename
			$data						//	template-data
		);		
		
	}		

	public function save_cmp( $id = -1 ) 
	{		
		if($_POST['save_cmp'] > 0)
		{
			$id = intval($_POST['save_cmp']);
		}
		
		
		if(isset($_POST['active']) && $_POST['active'] == 'on' )
		{
			$_POST['active'] = 1;
		}
		else
		{
			$_POST['active'] = 0;
		}

		$_POST['cmp'] = $_POST['cmp_id'];
		
		$oREQUEST = LEPTON_request::getInstance();	
	
		$all_names = array (
			'cmp'	=> array ('type' => 'integer', 'default' => -1),
			'consent_code'	=> array ('type' => 'string', 'default' => ""),
			'active'	=> array ('type' => 'integer', 'default' => 0),
		);		

		$all_values = $oREQUEST->testPostValues($all_names);		
		$table = TABLE_PREFIX."mod_cookiepro";
		$this->database->build_and_execute( 
			'UPDATE', 
			$table, 
			$all_values,
			'id = '.$id
		);

		foreach($this->all_cmp as $ref)
		{
			$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_cookiepro_cmp SET active = 0 ");
		}
		
		if($all_values['active'] == 1)
		{
			$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_cookiepro_cmp SET active = 1 WHERE cmp_id = ".$_POST['cmp_id']);
		}
		
		$this->admin->print_success($this->language['record_saved'], $this->action_url);
	}	
	
	
		
	
	public function show_info() 
	{
		// create links
		$support_link = "<a href=\"#\">NO Live-Support / FAQ</a>";	
		$readme_link = "<a href=\"https://cms-lab.com/_documentation/cookiepro/readme.php \" class=\"info\" target=\"_blank\">Readme</a>";	

		// data for twig template engine	
		$data = array(
			'oCP'			=> $this,
			'readme_link'	=> $readme_link,		
			'SUPPORT'		=> $support_link,	
			'image_url'		=> 'https://cms-lab.com/_documentation/media/cookiepro/cookiepro.jpg'
			);

		// get the template-engine	
		echo $this->oTwig->render(
			"@cookiepro/info.lte",	//	template-filename
			$data						//	template-data
		);		
		
	}	

} // end of class
