<?php

/**
 * @module          CookiePro
 * @author          cms-lab
 * @copyright       2019-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookiepro/license.php
 * @license_terms   please see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include ".SEC_FILE."!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


if(isset ($_GET['tool'])) 
{
	$toolname = $_GET['tool'];
} 
else 
{
	die('[1]');
}

// get instance of functions file
$oCP = cookiepro::getInstance();

			


if(isset ($_GET['tool']) && (empty($_POST)) || isset($_POST['back'])) {
	$oCP->list_cmp();
}
if(isset ($_POST['job']) && ($_POST['job'] == 'show_info') ) {
	$oCP->show_info();
}

if(isset ($_POST['edit_cmp']) && ($_POST['edit_cmp'] > 0) ) {
	$oCP->edit_cmp($_POST['edit_cmp']);
}

if(isset ($_POST['save_cmp']) && ($_POST['save_cmp'] != '') ) {
	$oCP->save_cmp($_POST['save_cmp']);
}
