<?php

/**
 * @module          CookiePro
 * @author          cms-lab
 * @copyright       2019-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/cookiepro/license.php
 * @license_terms   please see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include ".SEC_FILE."!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory     = "cookiepro";
$module_name          = "CookiePro";
$module_function      = "tool";
$module_version       = "1.5.0";
$module_platform      = "7.x";
$module_author        = '<a href="https://cms-lab.com" target="_blank">CMS-LAB</a>';
$module_license       = '<a href="https://cms-lab.com/_documentation/cookiepro/license.php" class="info" target="_blank">Custom license</a>';
$module_license_terms = "please see license";
$module_description   = "Tool to handle cookie consent management provider";
$module_guid		  = "31f8f8f9-2066-4cb0-bbdc-86f368dd80d8";
